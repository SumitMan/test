import {
  isNullUndefined,
  isNotNull,
  getQueryParams,
  stringToNumber,
  isArray,
  isObject,
  isEmpty,
  commaSepratedAmount,
  getAmountInWords,
  isNumber,
  toBoolean,
  searchEnumByValue,
  getEnumName,
  roundToPrecision,
  formatDate,
  getProperty,
  format12Hour,
  parseDate,
  multiLevelSort,
  poll,
  getDeepValue,
  SortDirection,
  add,
} from './misc';

test('add', () => {
  expect(add(2,3)).toBe(5);
});

describe('Tests for multiLevelSort', () => {
  const testData: any[] = [
    {
      parent1: 2,
      parent2: {
        child1: 'GHI',
        child2: {
          grandChild: 40,
        },
      },
    },
    {
      parent1: 3,
      parent2: {
        child1: 'JKL',
        child2: {
          grandChild: 30,
        },
      },
    },
    {
      parent1: 2,
      parent2: {
        child1: 'DEF',
        child2: {
          grandChild: 20,
        },
      },
    },
    {
      parent1: 1,
      parent2: {
        child1: 'ABC',
        child2: {
          grandChild: 10,
        },
      },
    },
  ];

  test('sort on 1 parameter', () => {
    const sortResult = multiLevelSort(testData, [
      {
        name: 'parent1',
        direction: SortDirection.Ascending,
      },
    ]);
    const expectedResult: any[] = [
      {
        parent1: 1,
        parent2: {
          child1: 'ABC',
          child2: {
            grandChild: 10,
          },
        },
      },
      {
        parent1: 2,
        parent2: {
          child1: 'GHI',
          child2: {
            grandChild: 40,
          },
        },
      },
      {
        parent1: 2,
        parent2: {
          child1: 'DEF',
          child2: {
            grandChild: 20,
          },
        },
      },
      {
        parent1: 3,
        parent2: {
          child1: 'JKL',
          child2: {
            grandChild: 30,
          },
        },
      },
    ];
    expect(sortResult).toEqual(expectedResult);
  });

  test('sort on 1 nested parameter', () => {
    const sortResult = multiLevelSort(testData, [
      {
        name: 'parent2.child2.grandChild',
        direction: SortDirection.Ascending,
      },
    ]);
    const expectedResult: any[] = [
      {
        parent1: 1,
        parent2: {
          child1: 'ABC',
          child2: {
            grandChild: 10,
          },
        },
      },
      {
        parent1: 2,
        parent2: {
          child1: 'DEF',
          child2: {
            grandChild: 20,
          },
        },
      },
      {
        parent1: 3,
        parent2: {
          child1: 'JKL',
          child2: {
            grandChild: 30,
          },
        },
      },
      {
        parent1: 2,
        parent2: {
          child1: 'GHI',
          child2: {
            grandChild: 40,
          },
        },
      },
    ];
    expect(sortResult).toEqual(expectedResult);
  });

  test('sort on 2 parameters', () => {
    const sortResult = multiLevelSort(testData, [
      {
        name: 'parent1',
        direction: SortDirection.Ascending,
      },
      {
        name: 'parent2.child2.grandChild',
        direction: SortDirection.Descending,
      },
    ]);
    const expectedResult: any[] = [
      {
        parent1: 1,
        parent2: {
          child1: 'ABC',
          child2: {
            grandChild: 10,
          },
        },
      },
      {
        parent1: 2,
        parent2: {
          child1: 'GHI',
          child2: {
            grandChild: 40,
          },
        },
      },
      {
        parent1: 2,
        parent2: {
          child1: 'DEF',
          child2: {
            grandChild: 20,
          },
        },
      },
      {
        parent1: 3,
        parent2: {
          child1: 'JKL',
          child2: {
            grandChild: 30,
          },
        },
      },
    ];
    expect(sortResult).toEqual(expectedResult);
  });

  test('sort on 2 parameters', () => {
    const sortResult = multiLevelSort(testData, [
      {
        name: 'parent2.child1',
        direction: SortDirection.Descending,
      },
      {
        name: 'parent2.child2.grandChild',
        direction: SortDirection.Descending,
      },
    ]);
    const expectedResult: any[] = [
      {
        parent1: 3,
        parent2: {
          child1: 'JKL',
          child2: {
            grandChild: 30,
          },
        },
      },
      {
        parent1: 2,
        parent2: {
          child1: 'GHI',
          child2: {
            grandChild: 40,
          },
        },
      },
      {
        parent1: 2,
        parent2: {
          child1: 'DEF',
          child2: {
            grandChild: 20,
          },
        },
      },
      {
        parent1: 1,
        parent2: {
          child1: 'ABC',
          child2: {
            grandChild: 10,
          },
        },
      },
    ];
    expect(sortResult).toEqual(expectedResult);
  });

  test('check when sorting list is empty', () => {
    expect(multiLevelSort([], [])).toEqual([]);
  });
});

describe('isNullUndefined', () => {
  test('null and undefined check', () => {
    expect(isNullUndefined(null)).toBe(true);
    expect(isNullUndefined(undefined)).toBe(true);
  });
  test('valid value check', () => {
    expect(isNullUndefined(3)).toBe(false);
  });
  test('truthy and falsy value check', () => {
    expect(isNullUndefined(false)).toBe(false);
    expect(isNullUndefined(true)).toBe(false);
    expect(isNullUndefined('')).toBe(false);
  });
});

describe('isNotNull', () => {
  test('null value check', () => {
    expect(isNotNull(null)).toBe(false);
  });
  test('valid value check', () => {
    expect(isNotNull(12)).toBe(true);
  });
  test('falsy value check', () => {
    expect(isNotNull('')).toBe(false);
    expect(isNotNull(undefined)).toBe(false);
  });
});

test('checks getQueryParams', () => {
  expect(getQueryParams('http://test.com?key=value&key2=value2')).toEqual({ key: 'value', key2: 'value2' });
  expect(getQueryParams('http://test.com')).toEqual({ '': 'undefined' });
});

test('checks stringToNumber', () => {
  expect(stringToNumber('test')).toBe('test');
  expect(stringToNumber('33')).toBe(33);
  expect(stringToNumber('123.45')).toBe(123);
  expect(stringToNumber('')).toBe(NaN);
});

test('checks isArray', () => {
  expect(isArray([])).toBe(true);
});

test('checks isObject', () => {
  expect(isObject({})).toBe(true);
});

test('checks isEmpty', () => {
  expect(isEmpty([])).toBe(true);
  expect(isEmpty({})).toBe(true);
  expect(isEmpty(2)).toBe(false);
  expect(isEmpty([1, 2, 3])).toBe(false);
  expect(isEmpty({ test: 'value' })).toBe(false);
});

test('checks commaSepratedAmount', () => {
  expect(commaSepratedAmount(100000)).toBe('1,00,000');
  expect(commaSepratedAmount(9999)).toBe('9,999');
  expect(commaSepratedAmount(-100000)).toBe('-1,00,000');
  expect(commaSepratedAmount(0)).toBe('0');
  expect(commaSepratedAmount(1234.123)).toBe('1,234.123');
});

test('checks getAmountInWords', () => {
  expect(getAmountInWords(100)).toBe('100');
  expect(getAmountInWords(1000)).toBe('1 Thousand');
  expect(getAmountInWords(100000)).toBe('1 Lakh');
  expect(getAmountInWords(10000000)).toBe('1 Crore');
  expect(getAmountInWords(100, 1, 1000)).toBe('100');
  expect(getAmountInWords(1000, 1, 1000, true)).toBe('1 K');
  expect(getAmountInWords(1000.678, 1, 1000)).toBe('1 Thousand');
});

test('checks isNumber', () => {
  expect(isNumber(null)).toBe(false);
  expect(isNumber('1000')).toBe(true);
  expect(isNumber(1000)).toBe(true);
  expect(isNumber('0')).toBe(true);
});

test('checks toBoolean', () => {
  expect(toBoolean(null)).toBe(null);
  expect(toBoolean(false)).toBe(false);
  expect(toBoolean(1)).toBe(true);
  expect(toBoolean('true')).toBe(true);
  expect(toBoolean(0)).toBe(false);
  expect(toBoolean('false')).toBe(false);
  expect(toBoolean(2)).toBeNull();
  expect(toBoolean('asd')).toBeNull();
});

enum TestEnum {
  Value1,
  Value2,
  Value3,
}
test('checks searchEnumByValue', () => {
  expect(searchEnumByValue<TestEnum, TestEnum>(TestEnum, TestEnum.Value1)).toBe(TestEnum.Value1);
});

test('checks getEnumName', () => {
  expect(getEnumName<TestEnum>(TestEnum, TestEnum.Value1)).toBe('Value1');
});

test('checks roundToPrecision', () => {
  expect(roundToPrecision(10.421231)).toBe(10);
  expect(roundToPrecision(10.965)).toBe(11);
  expect(roundToPrecision(9.5)).toBe(10);
  expect(roundToPrecision(10.42342, 4)).toBe(10);
});

test('checks formatDate', () => {
  expect(formatDate(new Date('2020-01-01'), 'MM-dd-yyyy')).toBe('01-01-2020');
  expect(formatDate(new Date('2020-01-02'), 'yyyy-dd-MM')).toBe('2020-02-01');
  expect(formatDate(new Date('2020-01-02'), 'yyyy')).toBe('2020');
});

test('checks getProperty', () => {
  expect(getProperty({}, 'key', 'default value')).toBe('default value');
  expect(getProperty({ key: 'value' }, 'key')).toBe('value');
  expect(getProperty({ item1: 'value1', item2: { item3: 'value3', item4: { item5: 'value5' } } }, 'item2.item4.item5')).toBe('value5');
  expect(getProperty({ item1: 'value1', item2: { item3: 'value3', item4: { item5: 'value5' } } }, 'item2.item8.item5')).toBeUndefined();
});

test('checks format12Hour', () => {
  expect(format12Hour(12)).toBe('12:00AM');
  expect(format12Hour(15)).toBe('3:00PM');
  expect(format12Hour(12, false)).toBe('12AM');
  expect(format12Hour(15, false)).toBe('3PM');
  expect(format12Hour(12, false, false)).toBe('12');
  expect(format12Hour(15, false, false)).toBe('3');
});

test('checks parseDate', () => {
  expect(parseDate('2020-01-01')).toBe('Wed Jan 01 2020');
});

describe('poll function', () => {
  test('poll with maximum attempts', async () => {
    let testValue = 0;
    function testFunction() {
      testValue++;
    }
    poll(testFunction, 0, 20);
    // wait function to let poll func finish first
    await new Promise((resolve) =>
      setTimeout(() => {
        resolve();
      }, 500)
    );
    expect(testValue).toBe(20);
  });
  test('poll interrupted before maximum attempts', async () => {
    let testValue = 0;
    function testFunction(intervalId: any) {
      if (testValue === 5) clearInterval(intervalId);
      else testValue++;
    }
    poll(testFunction, 0, 20);
    // wait function to let poll func finish first
    await new Promise((resolve) =>
      setTimeout(() => {
        resolve();
      }, 500)
    );
    expect(testValue).toBe(5);
  });
});

describe('getDeepValue', () => {
  const data = {
    item1: 'value1',
    item2: {
      item3: 123,
    },
  };
  test('check if nested value is fetched', () => {
    expect(getDeepValue(data, 'item2.item3')).toBe(123);
  });
  test('check if root properties are fetched', () => {
    expect(getDeepValue(data, 'item1')).toBe('value1');
  });
  test('check whether object is returned', () => {
    expect(getDeepValue(data, 'item2')).toEqual({ item3: 123 });
  });
  test('check for property which does not exist', () => {
    expect(getDeepValue(data, 'item4')).toBeUndefined();
  });
  test('check if source obj is undefined or null', () => {
    expect(getDeepValue(undefined, 'item2.item3')).toBeUndefined();
    expect(getDeepValue(null, 'item2.item3')).toBeNull();
  });
  test('check when property required is empty', () => {
    expect(getDeepValue(data, '')).toBeUndefined();
  });
  test('check when source data is a primitive type', () => {
    expect(getDeepValue('sample string', 'length')).toBe(13);
    expect(getDeepValue('sample string', 'item1')).toBeUndefined();
  });
  test('check when source data is an array', () => {
    expect(getDeepValue([], 'length')).toBe(0);
    expect(getDeepValue([], 'item1')).toBeUndefined();
  });
  test('check when property ends with .', () => {
    expect(getDeepValue(data, 'item2.')).toBeUndefined();
  });
});

