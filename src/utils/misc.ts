/* eslint-disable no-nested-ternary */
import { subYears, format } from 'date-fns';
export const SUFFIX_DIVISOR_MAP = {
  thousand: 1000,
  lakh: 100000,
  crore: 10000000,
};
export enum SortDirection {
  Ascending = 1,
  Descending = -1,
}
export interface IUnitLabel {
  full: string;
  short: string;
}
export interface ISortParam {
  name: string;
  direction: SortDirection;
}
/**
 * Returns true if data is null or undefined
 * @param data
 */
export const isNullUndefined = (data: any): boolean => {
  return data === null || data === undefined;
};

/**
 *
 * @param data
 */
export const isNotNull = (data: any): boolean => data !== undefined && data !== null && data !== '';

/**
 *
 *
 * @param {string} url
 * @returns {{ [key: string]: string }}
 */
export const getQueryParams = (url: string): { [key: string]: string } => {
  const queryParams = {};
  // create an anchor tag to use the property called search
  const anchor = document.createElement('a');
  // assigning url to href of anchor tag
  anchor.href = url;
  // search property returns the query string of url
  const queryStrings = anchor.search.substring(1);
  const params = queryStrings.split('&');

  for (let i = 0; i < params.length; i += 1) {
    const pair = params[i].split('=');
    (queryParams as any)[pair[0]] = decodeURIComponent(pair[1]);
  }
  return queryParams;
};

/**
 * Sets data to localStorage for key
 * removes key if data is null or undefined
 * converts data to string before storing
 * @param key
 * @param data
 */
export const setData = /* istanbul ignore next */ (key: string, data: any): void => {
  if (isNullUndefined(data)) {
    localStorage.removeItem(key);
  } else {
    const dataStr: string = typeof data === 'string' ? data : JSON.stringify(data);
    localStorage.setItem(key, dataStr);
  }
};

/**
 * Gets data from localStorage for key
 * Returns the Type specified in T
 * Returns null if data is null or undefined
 * @param key
 */
export const getData = /* istanbul ignore next */ <T>(key: string): T | string | null => {
  const data: string | null = localStorage.getItem(key);
  if (isNotNull(data)) {
    try {
      return JSON.parse(data as string) as T;
    } catch (e) {
      return data;
    }
  }
  return null;
};

/**
 * Removes data from localStorage for key
 * @param key localStorage key
 */

export const removeData = /* istanbul ignore next */ (key: string): void => {
  localStorage.removeItem(key);
};

/**
 *
 * @param string
 */
export const stringToNumber = (s: string): number => {
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(s as any)) {
    return s as any;
  }
  return parseInt(s, 10);
};

/**
 *
 * @param array
 */
export const isArray = (array: any[]): boolean => {
  return !!array && array.constructor === Array;
};

/**
 *
 * @param obj
 */
export const isObject = (obj: object): boolean => {
  return !!obj && obj.constructor === Object;
};

/**
 *
 * @param data
 */
export const isEmpty = (data: any): boolean => {
  if (isArray(data)) {
    return !data.length;
  }
  if (isObject(data)) {
    return !Object.keys(data).length;
  }
  return !data;
};

/**
 * Get comma separated value
 * @param value
 */
export const commaSepratedAmount = (value: number): string => {
  try {
    const decimal: string[] = value
      .toString()
      .trim()
      .split('.');
    const amount: string = decimal[0];
    let hundreds: string = amount.substring(amount.length - 3);
    const rest: string = amount.substring(0, amount.length - 3);
    hundreds = rest !== '' ? `,${hundreds}` : hundreds;
    return `${rest.replace(/\B(?=(\d{2})+(?!\d))/g, ',')}${hundreds}${decimal[1] ? `.${decimal[1]}` : ''}`;
  } catch (error) {
    /* istanbul ignore next */
    console.log(error);
    /* istanbul ignore next */
    return `${value}`;
  }
};

/**
 * Returns numeric number to words
 * @param amount Amount
 * @param precision
 */
export const getAmountInWords = (amount: number, precision = 1, factor = 1000, shortUnit = false): string => {
  if (amount < factor) {
    return commaSepratedAmount(amount);
  }
  const thousandSuffix: IUnitLabel = { short: 'k', full: 'thousand' };
  const lakhSuffix: IUnitLabel = { short: 'l', full: 'lakh' };
  const croreSuffix: IUnitLabel = { short: 'cr', full: 'crore' };
  /* istanbul ignore next */
  const suffix: IUnitLabel =
    amount < 100000 ? thousandSuffix : amount < 100000 ? lakhSuffix : amount < 10000000 ? lakhSuffix : croreSuffix;

  const divisor: number = (SUFFIX_DIVISOR_MAP as any)[suffix.full];
  const quotient: number = amount / divisor;
  const remainder: number = amount % divisor;
  const result: string = quotient.toFixed(remainder === 0 ? 0 : precision);

  const suffixLabel: string = shortUnit ? suffix.short : suffix.full;

  /* istanbul ignore next */
  return `${result && Number(result) ? commaSepratedAmount(Number(result)) : result} ${suffixLabel.replace(/^\w/, (c) =>
    // eslint-disable-next-line comma-dangle
    c.toUpperCase()
  )}`;
};

/**
 * Checks if value is number or not
 * @param obj
 */
export const isNumber = (value: string | number | null | undefined): boolean => {
  if (isNullUndefined(value)) {
    return false;
  }
  return !!(+(value as string | number) || +(value as string | number) === 0);
};

/**
 * Converts string 'true' and 'false' to boolean
 * Converts number '1' and '0' to boolean true and false respectively
 * Returns null if value is null or anything other than 'true', 'false', 0, 1
 * @param value
 */
export const toBoolean = (value: string | number | null | undefined | boolean): boolean | null => {
  if (isNullUndefined(value)) {
    return null;
  }
  if (typeof value === 'boolean') {
    return value;
  }
  if (typeof value === 'string') {
    return value === 'true' ? true : value === 'false' ? false : null;
  }
  /* istanbul ignore next */
  if (typeof value === 'number') {
    return value === 1 ? true : value === 0 ? false : null;
  }
  /* istanbul ignore next */
  return null;
};

/**
 * Get enum equivalent of value
 * @param enumList enum
 * @param value enum value to be searched
 */
export const searchEnumByValue = <E, V>(enumList: any, value: V): E | null => {
  for (const property of Object.keys(enumList)) {
    if (typeof property === 'string' && enumList[property] === value) {
      return enumList[property];
    }
  }
  /* istanbul ignore next */
  return null;
};

/**
 * Get enum property of enum value
 * @param enumList enum
 * @param value enum value to be searched
 */
export const getEnumName = <V>(enumList: any, value: V): string | null => {
  for (const property of Object.keys(enumList)) {
    if (typeof property === 'string' && enumList[property] === value) {
      return property;
    }
  }
  /* istanbul ignore next */
  return null;
};

/**
 * Returns value rounded to the precision given
 * @param value
 * @param precision 1 by default
 */
export const roundToPrecision = (value: number, precision = 1): number => {
  return Math.round((value * 10 ** precision) / 10 ** precision);
};

/**
 *
 * @param data
 */
export const objToStr = /* istanbul ignore next */ (data: object): string => {
  return Object.keys(data).reduce(
    (str, key) =>
      // prettier-ignore
      `${str}${key}: ${typeof (data as any)[key] === 'object' ? JSON.stringify((data as any)[key]) : (data as any)[key]
      }\n,,`,
    ','
  );
};
/**
 *
 * @param object
 * @param path
 * @param defaultValue
 */
export const getProperty = (object: object, path: string, defaultValue?: any): any => {
  return path.split('.').reduce((acc, key) => {
    if (acc && acc.constructor === Object && key in acc) {
      return (acc as any)[key];
    }
    return defaultValue;
  }, object);
};

/* istanbul ignore next */
export const format2: any = { month: 'long', year: 'numeric' };

/* istanbul ignore next */
export const DATE_FORMAT = 'YYYY-MM-DD';

export const dobRange = /* istanbul ignore next */ (age: number): any => {
  return {
    to: subYears(new Date(), age + 1),
    from: subYears(new Date(), age),
  };
};

/**
 *
 * @param hour
 * @param showMinutes
 * @param showSuffix
 */
export const format12Hour = (hour: number, showMinutes = true, showSuffix = true): string => {
  return hour < 13
    ? `${hour}${showMinutes ? ':00' : ''}${showSuffix ? 'AM' : ''}`
    : `${hour - 12}${showMinutes ? ':00' : ''}${showSuffix ? 'PM' : ''}`;
};

/**
 *
 * @param date
 */
export const formatDate = /* istanbul ignore next */ (date: any, dateFormat = 'YYYY-MM-DD'): string => {
  return format(date, dateFormat);
};

/**
 * Parses date
 * @param date Date in string format YYYY-MM-DD
 */
export const parseDate = (date: string): string => {
  /* expected format: YYYY-MM-DD  */
  try {
    const parts: string[] = date.split('-');
    const parsedDate: Date = new Date(+parts[0], +parts[1] - 1, +parts[2]);
    return parsedDate.toDateString();
  } catch (error) {
    /* istanbul ignore next */
    console.log(error);
    /* istanbul ignore next */
    return date;
  }
};

/**
 * Fetch value of property tree from nested object
 * eg: getDeepValue({a: 1, b: {c: 2}}, 'b.c') outputs 2
 * @param obj Any object (nested objects supported)
 * @param property string property names separated by '.'
 * @returns returns value of property in obj or undefined if not found
 */
export const getDeepValue = (obj: any, property: string): any | undefined => {
  if (obj === undefined || obj === null) {
    console.error('source object is undefined or null');
    return obj;
  }
  const properties: string[] = property.trim().split('.');
  /* istanbul ignore next */
  if (properties.length === 0) {
    console.error(`Invalid property ${property} of`, obj);
    return undefined;
  }
  const result: any = obj[properties[0]];
  if (result === undefined) {
    console.error(`Invalid property ${properties[0]} of`, obj);
    return undefined;
  }
  if (properties.length === 1) {
    return result;
  }
  return getDeepValue(result, properties.slice(1).join('.'));
};

/**
 * A sort comparator function to be used and implemented in multiLevelSort custom function
 * @param sortParamList Array of ISortParam
 * @param index
 * @param isCaseSensitive
 * @param a sorting param a
 * @param b sorting param b
 */
const multiLevelSortComparator = (
  sortParamList: ISortParam[],
  index: number,
  isCaseSensitive: boolean,
  a: any,
  b: any
): number => {
  // Check previous sort params values
  for (let i = 0; i < index; i++) {
    // Get variable value
    let prevParamA: any = getDeepValue(a, sortParamList[i].name);
    if (typeof prevParamA === 'string' && !isCaseSensitive) {
      prevParamA = prevParamA.toLowerCase();
    }
    // Get variable value
    let prevParamB: any = getDeepValue(b, sortParamList[i].name);
    if (typeof prevParamB === 'string' && !isCaseSensitive) {
      prevParamB = prevParamB.toLowerCase();
    }

    // Sorting logic for prev parameters
    if (prevParamA !== prevParamB) {
      return 0;
    }
  }

  // Current comparator
  // Get variable value
  let paramA: any = getDeepValue(a, sortParamList[index].name);
  if (typeof paramA === 'string' && !isCaseSensitive) {
    paramA = paramA.toLowerCase();
  }
  // Get variable value
  let paramB: any = getDeepValue(b, sortParamList[index].name);
  if (typeof paramB === 'string' && !isCaseSensitive) {
    paramB = paramB.toLowerCase();
  }

  // Sorting logic
  if (paramA > paramB) {
    return 1 * sortParamList[index].direction;
  }
  if (paramA < paramB) {
    return -1 * sortParamList[index].direction;
  }
  return 0;
};

/**
 * Sort list over multiple levels. eg: sort list on variable a, then sort on variable b but within group created by a
 * @param list Array of items to be sorted
 * @param sortParamList Array of ISortParam, which specifies variable to be sorted on and sort direction
 * @param isCaseSensitive false by default; applied only to string values
 */
export const multiLevelSort = <T = any>(list: T[], sortParamList: ISortParam[], isCaseSensitive = false): T[] => {
  const sortList: T[] = [...list];
  if (!sortList.length) {
    return sortList;
  }
  for (let index = 0; index < sortParamList.length; index++) {
    sortList.sort(multiLevelSortComparator.bind(sortList, sortParamList, index, isCaseSensitive));
  }
  return sortList;
};

/**
 * Calls function for fixed number of times, which can be interupted
 * @param functionToPoll Function to poll
 * @param interval Within how much interval to keep polling at in ms
 * @param maxAttempts Maximum number of attempts to poll for
 */
export const poll = (
  functionToPoll: (intervalId: number) => void | Promise<void>,
  /* istanbul ignore next */
  interval = 3000,
  /* istanbul ignore next */
  maxAttempts = 20
): void => {
  let attempts = 0;
  const intervalId: any = setInterval(() => {
    if (attempts >= maxAttempts) {
      clearInterval(intervalId);
    } else {
      attempts++;
      functionToPoll(intervalId);
    }
  }, interval);
};

export const add = (a: number, b: number): number => {
  return a + b;
};