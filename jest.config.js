module.exports = {
  moduleFileExtensions: ['ts', 'js'],
  transformIgnorePatterns: ['/node_modules/'],
  testPathIgnorePatterns: ['/tests/'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  testMatch: ['**/*.spec.(js|ts)'],
  globals: {
    'ts-jest': {
      babelConfig: true,
    },
  },
  preset: 'ts-jest',
  collectCoverage: true,
  collectCoverageFrom: [
    'src/utils/*.ts',
    '!**/node_modules/**',
    `!/tests/`,
  ],
  coverageReporters: ['html', 'text', 'json'],
  coverageDirectory: 'unitTests/coverage',
  coverageThreshold: {
    global: {
      branches: 70,
      functions: 90,
      lines: 87,
      statements: 87,
    },
  },
};
